import asyncio

try:
    from socket import socketpair
except ImportError:
    from asyncio.windows_utils import socketpair

rsock, wsock = socketpair()
loop = asyncio.get_event_loop()


def reader():
    data = rsock.recv(100)
    print("Received:", data.decode())
    # we are done: unregister the file descriptor
    loop.remove_reader(rsock)
    # stop the event loop
    loop.stop()


loop.add_reader(rsock, reader)

loop.call_soon(wsock.send, 'abc'.encode())
loop.run_forever()

rsock.close()
wsock.close()
loop.close()
